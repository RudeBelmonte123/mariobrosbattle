﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour
{
    private GameObject canoEsquerda;
    private bool isFacingRight;
    private float speed;

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        Speed = 1f;
        canoEsquerda = GameObject.FindGameObjectWithTag("CanoEsquerda");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
        RespawnSpike();

        if(transform.position.x <= -2.0f && transform.position.y <= 1.77f)
        {
            this.GetComponent<Rigidbody2D>().simulated = false;
            canoEsquerda.GetComponent<Animator>().SetBool("SaindoMonstro", true);
        }else{
            this.GetComponent<Rigidbody2D>().simulated = true;
            canoEsquerda.GetComponent<Animator>().SetBool("SaindoMonstro", false);
        }
    }
    
    private void RespawnSpike()
    {
        // if get to the screen limit and it is facing right and it's under -1.0f of Y position then do it
        if(transform.position.x >= 4.7f && transform.position.y <= -1.0f)
        {
            transform.position = new Vector3(-4.2f, -3.0f, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "BoxLeft")
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if(col.gameObject.tag == "BoxRight")
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
}

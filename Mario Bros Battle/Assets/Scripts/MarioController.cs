﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarioController : MonoBehaviour
{
    private float jumpForce = 450f;
    private bool isFacingRight;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveMario();
        Jump();
        BoolFunction();
        FlipX();
        Animation();
    }

    private void Jump()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector3.up * jumpForce, ForceMode2D.Impulse);
        }
    }

    private void FlipX()
    {

        if(isFacingRight)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }

        if(!isFacingRight)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
    }

    private void BoolFunction()
    {
        float HorizontalMove = Input.GetAxis("Horizontal");

        if(HorizontalMove >= 0.01f)
        {
            isFacingRight = true;
        }
        if(HorizontalMove <= -0.01f)
        {
            isFacingRight = false;
        }
    }
    private void MoveMario()
    {   
        float HorizontalMove = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * 2f * Time.deltaTime * HorizontalMove);
    }

    private void Animation()
    {
        float HorizontalMove = Input.GetAxis("Horizontal");

        if(HorizontalMove >= 0.01f)
        {
            this.GetComponent<Animator>().SetBool("walk", true);
        }else if(HorizontalMove <= -0.01f)
        {
            this.GetComponent<Animator>().SetBool("walk", true);
        }else{
            this.GetComponent<Animator>().SetBool("walk", false);
        }
    }
}

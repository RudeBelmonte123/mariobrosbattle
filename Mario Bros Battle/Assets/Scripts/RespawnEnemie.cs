﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnEnemie : MonoBehaviour
{
    public GameObject spike;
    private Vector3 setPosition = new Vector3(-2.95f, 1.60f, 0);
    private float timeRate = 0f;
    private float spawnRate = 5f;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = setPosition;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("spawnEnemie");
    }

    IEnumerator spawnEnemie()
    {
        yield return new WaitForSeconds(5f);
        // algorithim for instantiate an enemie with a pre-determined time
        if(Time.time >= timeRate)
        {
            timeRate = timeRate + spawnRate;
            Instantiate(spike, transform.position, Quaternion.identity);
        }
    }
}
